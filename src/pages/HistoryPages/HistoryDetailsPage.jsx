import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { historyActions } from "../../store/actions/history.action";

function HistoryDetailsPage(props) {
  const history = useSelector((state) => state.history.historyList);
  const details = useSelector((state) => state.history.historyDetails);
  const detailsID = props.location.hash.slice(1);
  const dispatch = useDispatch();
  let detailsArray = [];

  let historyDetails = history
    ? history.filter((el) => el._id == detailsID)
    : detailsArray;

  useEffect(() => {
    if (!props.location.hash) {
      dispatch(historyActions.getHistoryDetails());
    }
  }, [props.location.hash]);
  
  detailsArray.push(details);

  return (
    <div>
      <ul>
        {(details || history) &&
          historyDetails.map(({ city, date, info }) => {
            return (
              <div key={Math.random()}>
                <p style={{ paddingLeft: "12px" }}>
                  <b>City</b> : {city}
                </p>
                <p style={{ paddingLeft: "12px" }}>
                  <b>Time</b> : {new Date(date).toLocaleString()}
                </p>
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Temperature</th>
                      <th>Condition</th>
                    </tr>
                  </thead>
                  <tbody>
                    {info.map(({ date, temp, weather }) => {
                      return (
                        <tr key={date + temp}>
                          <td>{new Date(date).toLocaleString()}</td>
                          <td> {temp > 0 ? `+${temp}` : temp}</td>
                          <td>{weather}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            );
          })}
      </ul>
    </div>
  );
}

export { HistoryDetailsPage };
