import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { historyActions } from "../../store/actions/history.action";
import { HashLink } from "react-router-hash-link";

function HistoryListPage() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.authentication.user);
  const history = useSelector((state) => state.history.historyList);
  const { id } = user;
  const params = {
    id,
    page: 3,
  };
  useEffect(() => {
    dispatch(historyActions.getHistoryList(params));
  }, []);

  return (
    <div>
      <ul>
        {history &&
          history.map(({ city, date, _id }) => {
            return (
              <li style={{ listStyleType: "none" }} key={city + date}>
                <HashLink to={`/history-details#${_id}`}>
                  {new Date(date).toLocaleString()} - {city}
                </HashLink>
              </li>
            );
          })}
      </ul>
    </div>
  );
}

export { HistoryListPage };
