import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import CheckIcon from "@material-ui/icons/Check";
import { userActions } from "../../store/actions";

function ProfilePage() {
  const infoUser = useSelector((state) => state.authentication.user);
  const dispatch = useDispatch();

  const [user, setUser] = useState({
    username: infoUser.username,
    email: infoUser.email,
    password: infoUser.email,
  });

  const [inputDisable, setInputDisable] = useState({
    username: true,
    password: true,
  });

  const [flagEdit, setFlagEdit] = useState({
    password: false,
    username: false,
  });

  const [submitted, setSubmitted] = useState(false);

  function handleChange(e) {
    const { name, value } = e.target;
    setUser((user) => ({ ...user, [name]: value }));
    if (name == "password") {
      setFlagEdit((data) => ({ ...data, password: true }));
    } else {
      setFlagEdit((data) => ({ ...data, username: true }));
    }
  }

  function handleClick(nameInput, flag) {
    setInputDisable((input) => ({
      ...input,
      [nameInput]: !inputDisable[nameInput],
    }));

    if (!flag) {
      infoUser[nameInput] &&
        setUser((user) => ({ ...user, [nameInput]: infoUser[nameInput] }));
      setFlagEdit((data) => ({ ...data, [nameInput]: false }));
      setUser((user) => ({ ...user, password: infoUser.email }));
    } else {
      setSubmitted(true);
    }
  }

  function handleSubmit(e) {
    e.preventDefault();

    let userinfo = {
      username: user.username,
      email: user.email,
      _id: infoUser.id,
    };


    flagEdit.password && Object.assign(userinfo, { password: user.password });
    dispatch(userActions.update(userinfo));
  }

  return (
    <div className="col-lg-8 offset-lg-2">
      <h2>Profile</h2>
      <form name="form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Username</label>
          <div className="d-flex align-items-center">
            <input
              type="text"
              name="username"
              style={{ width: "80%" }}
              value={user.username}
              onChange={handleChange}
              disabled={inputDisable.username}
              className={"form-control" + (!user.username ? " is-invalid" : "")}
            />
            {inputDisable.username && (
              <EditIcon
                style={{ cursor: "pointer" }}
                onClick={() =>
                  setInputDisable((input) => ({
                    ...input,
                    username: !inputDisable.username,
                  }))
                }
                className="ml-2"
              />
            )}
            {!inputDisable.username && (
              <CloseIcon
                style={{ color: "red", cursor: "pointer" }}
                onClick={() => handleClick("username", false)}
                className="ml-2"
              />
            )}
            {user.username && !inputDisable.username && (
              <CheckIcon
                style={{ color: "green", cursor: "pointer" }}
                onClick={() => handleClick("username", true)}
                className="ml-2"
              />
            )}
          </div>
          {!user.username && (
            <div style={{ display: "block" }} className="invalid-feedback">
              Username is required
            </div>
          )}
        </div>
        <div className="form-group">
          <label>Password</label>
          <div className="d-flex align-items-center">
            <input
              type="password"
              name="password"
              style={{ width: "80%" }}
              value={user.password}
              onChange={handleChange}
              disabled={inputDisable.password}
              className={"form-control" + (!user.password ? " is-invalid" : "")}
            />
            {inputDisable.password && (
              <EditIcon
                style={{ cursor: "pointer" }}
                onClick={() =>
                  setInputDisable((input) => ({
                    ...input,
                    password: !inputDisable.password,
                  }))
                }
                className="ml-2"
              />
            )}
            {!inputDisable.password && (
              <CloseIcon
                style={{ color: "red", cursor: "pointer" }}
                onClick={() => handleClick("password", false)}
                className="ml-2"
              />
            )}
            {user.password.length >= 5 && !inputDisable.password && (
              <CheckIcon
                style={{ color: "green", cursor: "pointer" }}
                onClick={() => handleClick("password", true)}
                className="ml-2"
              />
            )}
          </div>
          {user.password.length < 5 && (
            <div style={{ display: "block" }} className="invalid-feedback">
              A password must contain 5 to 10 characters.
            </div>
          )}
        </div>

        <div className="form-group">
          <label>Email</label>
          <input
            type="text"
            name="email"
            style={{ width: "80%" }}
            value={user.email}
            disabled
            onChange={handleChange}
            className={"form-control" + (!user.email ? " is-invalid" : "")}
          />
          {!user.email && (
            <div className="invalid-feedback">Email is required</div>
          )}
        </div>

        <div className="form-group">
          <button disabled={!submitted} className="btn btn-primary">
            Update
          </button>
        </div>
      </form>
    </div>
  );
}

export { ProfilePage };
