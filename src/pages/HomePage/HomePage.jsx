import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Autocomplete from "react-google-autocomplete";
import { weatherAction } from "../../store/actions/weather.action";

function HomePage() {
  const user = useSelector((state) => state.authentication.user);
  const dispatch = useDispatch();
  const weatherList = useSelector((state) => state.weather.info);

  const inputEl = useRef(null);
  const [table, setTable] = useState(false);

  useEffect(() => {
    if (!inputEl.current.refs.input.value) {
      setTable(false);
    }
  }, []);

  return (
    <div>
      <Autocomplete
        className="form-control"
        ref={inputEl}
        onPlaceSelected={(place) => {
          if (!place.geometry) {
            return;
          }
          setTable(true);
          const lat = place.geometry.location.lat();
          const lng = place.geometry.location.lng();
          const city = place.formatted_address;

          dispatch(weatherAction.getWeather(lat, lng, city));
        }}
      />
      {table && (
        <table className="table table-hover">
          <thead className="">
            <tr>
              <th>Date</th>
              <th>Temperature</th>
              <th>Condition</th>
            </tr>
          </thead>
          <tbody>
            {weatherList &&
              weatherList.map(({ date, temp, weather }) => {
                return (
                  <tr key={date + temp}>
                    <td>{new Date(date).toLocaleString()}</td>
                    <td> {temp > 0 ? `+${temp}` : temp}</td>
                    <td>{weather}</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      )}
    </div>
  );
}

export { HomePage };
