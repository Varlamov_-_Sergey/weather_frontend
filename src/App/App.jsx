import React, { useEffect } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { history } from "../_helpers";
import { alertActions } from "../store/actions";
import { PrivateRoute } from "../_components";
import { HomePage } from "../pages/HomePage";
import { LoginPage } from "../pages/LoginPage";
import { RegisterPage } from "../pages/RegisterPage";
import { HistoryListPage } from "../pages/HistoryPages";
import { HistoryDetailsPage } from "../pages/HistoryPages";
import { NavbarMenu } from "../shared/NavbarMenu";
import { ProfilePage } from "../pages/ProfilePage";

function App() {
  const alert = useSelector((state) => state.alert);
  const dispatch = useDispatch();

  useEffect(() => {
    history.listen((location, action) => {
      dispatch(alertActions.clear());
    });
  }, []);

  return (
    <div className="jumbotron mb-0 pb-0">
      <div className="container">
        <div className="col-md-8 offset-md-2">
          {alert.message && (
            <div className={`alert ${alert.type}`}>{alert.message}</div>
          )}
          <Router history={history}>
            <NavbarMenu />
            <Switch>
              <PrivateRoute exact path="/profile" component={ProfilePage} />
              <PrivateRoute exact path="/" component={HomePage} />
              <PrivateRoute exact path="/history" component={HistoryListPage} />
              <PrivateRoute
                exact
                path="/history-details"
                component={HistoryDetailsPage}
              />
              <Route path="/login" component={LoginPage} />
              <Route path="/register" component={RegisterPage} />
              <Redirect from="*" to="/" />
            </Switch>
          </Router>
        </div>
      </div>
    </div>
  );
}

export { App };
