import { authHeader } from '../_helpers';


const baseUrl = "http://localhost:4000"

export const weatherService = {
    getWeather,
};

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}
function getWeather(lat, lng, city) {

    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ lat, lng, city })
    };
    return fetch(`${baseUrl}/weather`, requestOptions)
        .then(handleResponse)
        .then(data => data);

}
