import { authHeader } from '../_helpers';


const baseUrl = "http://localhost:4000"

export const historyService = {
    getHistory,
    getHistoryDetails
};


function getHistory(params) {

    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };
    return fetch(`${baseUrl}/history/${params.id}`, requestOptions)
        .then(handleResponse)
        .then(data => data);

}


function getHistoryDetails() {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
    };
    return fetch(`${baseUrl}/history-details`, requestOptions)
        .then(handleResponse)
        .then(data => data );

}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}


function logout() {
    localStorage.removeItem('user');
}