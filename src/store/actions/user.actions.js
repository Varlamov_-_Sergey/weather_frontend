import { userConstants } from '../constants';
import { userService } from '../../services';
import { alertActions } from '.';
import { history } from '../../_helpers';

export const userActions = {
    login,
    logout,
    register,
    update,

};

function login(email, password) {
    return dispatch => {
        dispatch(request({ email }));
        userService.login(email, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function update (user) {

    return dispatch =>{
        dispatch(request());
        userService.update(user).
        then(
            user => {
                dispatch(success(user))
                dispatch(alertActions.success('Updated profile successful'));
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
        )

    }

        function request() { return { type: userConstants.UPDATE_REQUEST } }
        function success(user) { return { type: userConstants.LOGIN_UPDATE, user } }
        function failure(error) { return { type: userConstants.UPDATE_FAILURE, error } }

}


