import { weatherConstants } from '../constants';
import { alertActions } from '.';
import { weatherService } from '../../services'

export const weatherAction = {
    getWeather,

};

function getWeather(lat, lng, city) {
    return dispatch => {
        dispatch(request());
        weatherService.getWeather(lat, lng, city)
            .then(
                data => { 
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: weatherConstants.GET_WEATHER_REQUEST } }
    function success(data) { return { type: weatherConstants.GET_WEATHER_SUCCESS, data } }
    function failure(error) { return { type: weatherConstants.GET_WEATHER_FAILURE, error } }
}



