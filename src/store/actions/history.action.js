
import { historyConstants } from '../constants';
import { historyService } from '../../services';
import { alertActions } from '.';

export const historyActions = {
    getHistoryList,
    getHistoryDetails
};


function getHistoryList(params) {

    return dispatch => {

        dispatch(request());
        historyService.getHistory(params)
            .then(
                data => {
                    dispatch(success(data)); 
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );

    }

    function request() { return { type: historyConstants.HISTORY_LIST_REQUEST } }
    function success(data) { return { type: historyConstants.HISTORY_LIST_SUCCESS, data } }
    function failure(error) { return { type: historyConstants.HISTORY_LIST_FAILURE, error } }
}

function getHistoryDetails () {

        return dispatch => {

            dispatch(request());
            historyService.getHistoryDetails()
            .then(
                data => {
           
                    dispatch(success(data));
                    
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
        }
        function request() { return { type: historyConstants.HISTORY_DETAILS_REQUEST } }
        function success(data) { return { type: historyConstants.HISTORY_DETAILS_SUCCESS, data } }
        function failure(error) { return { type: historyConstants.HISTORY_DETAILS_FAILURE, error } }
}