import { combineReducers } from 'redux';
import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { weather } from './weather.reducer';
import { history } from './history.reducer';

const rootReducer = combineReducers({
    authentication,
    registration,
    users,
    alert,
    weather,
    history
});

export default rootReducer;