import { historyConstants } from '../constants';



export function history(state = {}, action) {
    switch (action.type) {
        case historyConstants.HISTORY_LIST_REQUEST:
            return {
                loading: true
            };
        case historyConstants.HISTORY_LIST_SUCCESS:
            return {
                ...state,
                historyList: action.data
            };
        case historyConstants.HISTORY_LIST_FAILURE:
            return {
                error: action.error
            };

        case historyConstants.HISTORY_DETAILS_REQUEST:
            return {
                loading: true
            };
        case historyConstants.HISTORY_DETAILS_SUCCESS:
            return {
                ...state,
                historyDetails: action.data
            };
        case historyConstants.HISTORY_DETAILS_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}