import { weatherConstants } from '../constants';

const initialState = {
  city: '',
  info: [],
  date: ''
};


export function weather(state = initialState, action) {
    switch (action.type) {
        case weatherConstants.GET_WEATHER_REQUEST:
            return {
                loading: true
            };
        case weatherConstants.GET_WEATHER_SUCCESS:
            return {
              ...state,
              city:action.data.city,
              info:action.data.info,
              data:action.data.date
            };
        case weatherConstants.GET_WEATHER_FAILURE:
            return {
                error: action.error
            };
            default:
              return state;
        }
        
    }


