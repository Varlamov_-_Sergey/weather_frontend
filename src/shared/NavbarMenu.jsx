
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  Button
} from 'reactstrap';
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

const NavItemStyled = styled(NavItem)`
  padding: 0.5rem;
`;

const LinkStyled = styled(Link)`
  color: darkblue;

  &:hover {
    text-decoration: none;
    color: darkslateblue;
  }
`;


function NavbarMenu() {

  const user = useSelector(state => state.authentication.user);
  
 
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
          <Navbar color="light"  style={ {listStyleType: 'none'}} className="mb-3" light expand="md">
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen ={ isOpen } navbar>
              <Nav className="w-100 justify-content-around" navbar>
                <NavItemStyled>
                  <LinkStyled to="/profile">Profile</LinkStyled>
                </NavItemStyled>
                <NavItemStyled>
                  <LinkStyled to="/">Weather</LinkStyled>
                </NavItemStyled>
                <NavItemStyled>
                  <LinkStyled to="/history">History List</LinkStyled>
                </NavItemStyled>
                <NavItemStyled>
                  <LinkStyled to="/history-details">History Details</LinkStyled>
                </NavItemStyled>
              </Nav>
            </Collapse>
            {user && user.username && <NavItemStyled>
              <LinkStyled to="/login">  <Button >Logout</Button></LinkStyled>
                </NavItemStyled>}
          </Navbar>
        </div>
      );
}

export { NavbarMenu };
